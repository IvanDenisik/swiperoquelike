﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Entitas;
using System.Linq;

public class BuffCharacterSystem : IExecuteSystem
{
    //Система ловит события и улучшает персонажей игрока

    readonly GameContext gameContext;
    readonly InputContext inputContext;
    readonly IGroup<GameEntity> characterBuffs;
    readonly IGroup<InputEntity> characterBuffEvents;

    public BuffCharacterSystem(Contexts contexts)
    {
        gameContext = contexts.game;
        inputContext = contexts.input;

        characterBuffs = gameContext.GetGroup(GameMatcher.CharacterBuff);
        characterBuffEvents = inputContext.GetGroup(InputMatcher.CharacterBuffEvent);
    }

	public void Execute()
    {
        foreach (var e in characterBuffEvents)
        {
            var buff = characterBuffs.GetEntities().ToList().Find(b => b.characterBuff.id == e.characterBuffEvent.id);
            var buffComp = buff.characterBuff;
            var ev = e.characterBuffEvent;
            buff.ReplaceCharacterBuff(ev.id, buffComp.might + ev.might, buffComp.armor + ev.armor, buffComp.counters + ev.counters, buffComp.abilityPower + ev.abilityPower);
        }
    }
}

[Input]
public class CharacterBuffEventComponent : IComponent
{
    //Класс описывает события, которые усиливают персонажей.

    public string id;
    public int might;
    public int armor;
    public int counters;
    public int abilityPower;
}