﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Entitas;
using Entitas.Unity;
using TMPro;
using DG.Tweening;

public class CharacterView : MonoBehaviour, IView, ICoordinateListener, IDestroyedListener, ICharacterListener
{
    [SerializeField] TextMeshProUGUI mightText;
    [SerializeField] TextMeshProUGUI countersText;
    [SerializeField] GameObject hintObject;

    IGameConfig config;

    Vector2Int lastCoordinate;

    public virtual void Link(IEntity entity)
    {
        gameObject.Link(entity);
        var e = (GameEntity)entity;
        e.AddDestroyedListener(this);
        e.AddCoordinateListener(this);
        e.AddCharacterListener(this);

        config = Contexts.sharedInstance.game.gameConfig.value;

        lastCoordinate = e.coordinate.coordinate;
        transform.localPosition = CoordToPosition(e.coordinate.coordinate);
        mightText.text = e.character.might.ToString();
        if (countersText != null)
            countersText.text = e.character.counters.ToString();
    }

    Vector2 CoordToPosition (Vector2Int coordinate)
    {
        return new Vector2(
            (coordinate.x - config.boardSize.x * 1.0f / 2 + 0.5f) * config.cellSize.x,
            (coordinate.y - config.boardSize.y * 1.0f / 2 + 0.5f) * config.cellSize.y);
    }

    public virtual void OnDestroyed(GameEntity entity, int framesTillDestroyed)
    {
        if (framesTillDestroyed > 1) return;

        transform.DOScale(0, 0.5f);
        Invoke("DestroyView", 0.5f);
        gameObject.Unlink();
    }

    void DestroyView()
    {       
        Destroy(gameObject);
    }

    public virtual void OnCoordinate(GameEntity entity, Vector2Int coordinate)
    {
        transform.DOLocalMove(CoordToPosition(coordinate), (lastCoordinate - coordinate).magnitude * config.swipeSpeed);
        lastCoordinate = coordinate;
    }

    public virtual void OnCharacter(GameEntity e, string id, bool playerOwnsCharacter, int might, int armor, 
        int counters, int abilityPower, int reward, int damageTaken, int healTaken)
    {
        mightText.text = might.ToString();
        if (countersText != null)
            countersText.text = counters.ToString();
    }
}