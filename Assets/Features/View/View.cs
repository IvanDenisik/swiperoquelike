﻿using Entitas;
using Entitas.Unity;
using UnityEngine;

public class View : MonoBehaviour, IView, ICoordinateListener, IDestroyedListener
{
    public virtual void Link(IEntity entity)
    {
        gameObject.Link(entity);
        var e = (GameEntity)entity;
        e.AddDestroyedListener(this);

        var config = Contexts.sharedInstance.game.gameConfig.value;
        
        transform.localPosition = new Vector2(
            (e.coordinate.coordinate.x - config.boardSize.x * 1.0f / 2 + 0.5f) * config.cellSize.x,
            (e.coordinate.coordinate.y - config.boardSize.y * 1.0f / 2 + 0.5f) * config.cellSize.y);
    }

    public virtual void OnDestroyed(GameEntity entity, int framesTillDestroyed)
    {
        if (framesTillDestroyed > 1) return;

        gameObject.Unlink();
        Destroy(gameObject);
    }

    public virtual void OnCoordinate(GameEntity entity, Vector2Int coordinate)
    {

    }
}
