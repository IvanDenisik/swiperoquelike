﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Entitas;
using Entitas.CodeGeneration.Attributes;

public class TapForNextRoomView : MonoBehaviour, IPointerClickHandler, IGameStateListener {

	void Start () {
        gameObject.SetActive(false);
        Contexts.sharedInstance.game.gameStateEntity.AddGameStateListener(this);
    }

    public void OnGameState(GameEntity entity, GameStateValue state)
    {
        if (state == GameStateValue.RoomEnded)
        {
            gameObject.SetActive(true);
        }
    }
	
	public void OnPointerClick(PointerEventData eventData)
    {
        Contexts.sharedInstance.input.CreateEntity().AddNextRoomTap(true);
        gameObject.SetActive(false);
    }
}

[Input, Unique]
public class NextRoomTapComponent : IComponent
{
    public bool needNextRoom;
}