﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class HintView : MonoBehaviour, IPointerClickHandler, IAnySwipeListener, IShowHintEventListener {

    //Класс описывающий логику представления - подсказка для персонажа. Появляется когда игрок тапает по персонажу на поле.

    [SerializeField] TextMeshProUGUI textField;
    [SerializeField] List<string> characterIds;
    [SerializeField] List<string> characterDescriptions;

    void Start()
    {
        Contexts.sharedInstance.game.showHintEventEntity.AddShowHintEventListener(this);
        gameObject.SetActive(false);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
    
    }

    public void OnAnySwipe(InputEntity entity, Vector2 from, Vector2 to)
    {
      
    }

    public void OnShowHintEvent(GameEntity entity, bool showHint, GameEntity entityToShow)
    {
        if (showHint)
        {
            var index = characterIds.FindIndex(id => id == entityToShow.character.id);
            if (index != -1)
            {
                textField.text = characterDescriptions[index];
                gameObject.SetActive(true);
            }
            else
                gameObject.SetActive(false);
        }
        else
            gameObject.SetActive(false);
    }
}
