﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Entitas;
using Entitas.CodeGeneration.Attributes;

public class TapForNewGameView : MonoBehaviour, IPointerClickHandler, IGameStateListener
{
    void Start()
    {
        gameObject.SetActive(false);
        Contexts.sharedInstance.game.gameStateEntity.AddGameStateListener(this);
    }

    public void OnGameState(GameEntity entity, GameStateValue state)
    {
        if (state == GameStateValue.GameEnded)
        {
            gameObject.SetActive(true);
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        Contexts.sharedInstance.input.CreateEntity().AddNewGameTap(true);
        gameObject.SetActive(false);
    }
}

[Input, Unique]
public sealed class NewGameTapComponent : IComponent
{
    public bool needNewGame;
}