﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Entitas;
using Entitas.Unity;
using Entitas.CodeGeneration.Attributes;

public class ShowHintEventCreator : MonoBehaviour, IPointerClickHandler
{
    public void OnPointerClick(PointerEventData eventData)
    {
        Contexts.sharedInstance.game.ReplaceShowHintEvent(true, (GameEntity) gameObject.GetEntityLink().entity);
    }
}

[Game, Unique, Event(EventTarget.Self)]
public sealed class ShowHintEventComponent : IComponent
{
    public bool showHint;
    public GameEntity entityToShow;
}