﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Entitas;
using Entitas.CodeGeneration.Attributes;
using UnityEngine.UI;

public class SwipeCreatorBehaviour : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    //Класс отлавливает свайпы и создает сущность для свайпа Swipe.
    //Скрипт надо повесить на объект в иерархии, который может принимать события IPointerDownHandler, IPointerUpHandler.

    [SerializeField] bool needCanvasBehaviour;

    Vector2 startPoint;

    void Start()
    {
        if (!needCanvasBehaviour)
            GetComponent<Image>().enabled = false;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        startPoint = eventData.position;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if ((startPoint - eventData.position).magnitude >= Contexts.sharedInstance.game.gameConfig.value.swipeMinLength)
        {
            Contexts.sharedInstance.input.CreateEntity()
                .AddSwipe(startPoint, eventData.position);
        }
    }

    void Update()
    {
        if (needCanvasBehaviour) return;

        if (Input.GetMouseButtonDown(0))
        {
            startPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }

        if (Input.GetMouseButtonUp(0))
        {
            Vector2 endPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            if ((startPoint - endPoint).magnitude >= Contexts.sharedInstance.game.gameConfig.value.swipeMinLength)
            {
                Contexts.sharedInstance.input.CreateEntity()
                    .AddSwipe(startPoint, endPoint);
            }
        }
    }
}

[Input, Unique, Event(EventTarget.Any)]
public sealed class SwipeComponent : IComponent, ISwipe
{
    public Vector2 startPointerPos { get; set; }
    public Vector2 endPointerPos { get; set; }
}

public interface ISwipe
{
    Vector2 startPointerPos { get; set; }
    Vector2 endPointerPos { get; set; }
}