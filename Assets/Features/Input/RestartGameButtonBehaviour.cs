﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RestartGameButtonBehaviour : MonoBehaviour {

	void Start () {
        GetComponent<Button>().onClick.AddListener(RestartGame);
    }
	
	void RestartGame () {
        Contexts.sharedInstance.input.CreateEntity().AddNewGameTap(true);
    }
}
