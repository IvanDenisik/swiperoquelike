﻿using Entitas;

public class ChangeGameStateSystem : IInitializeSystem, IExecuteSystem
{
    //Система меняет состояние игры, есои нужно. Часть переключения состояний вынесена в скрипты для интерфейсов.

    readonly GameContext context;
    readonly InputContext inputContext;
    readonly IGroup<GameEntity> characters;


    public ChangeGameStateSystem(Contexts contexts)
    {
        context = contexts.game;
        inputContext = contexts.input;
        characters = context.GetGroup(GameMatcher.Character);
    }

    public void Initialize()
    {
        if (context.gameState.state == GameStateValue.GameStarted)
            context.ReplaceGameState(GameStateValue.RoomStarted);
    }

    public void Execute()
    {            
        if (context.gameState.state == GameStateValue.RoomPlaying)
        {
            //Если на поле нет персонажей противника, то запускаем новую комнату
            bool hasEnemies = false;
            foreach (var character in characters)
                if (!character.character.playerOwnsCharacter)
                {
                    hasEnemies = true;
                    break;
                }
            if (!hasEnemies)
            {
                context.ReplaceGameState(GameStateValue.RoomEnded);
                return;
            }

            //Если на поле нет персонажей игрока, то перезапускаем игру
            bool hasAllies = false;
            foreach (var character in characters)
                if (character.character.playerOwnsCharacter)
                {
                    hasAllies = true;
                    break;
                }
            if (!hasAllies)
            {
                context.ReplaceGameState(GameStateValue.GameEnded);
                return;
            }
        }

        if (inputContext.nextRoomTapEntity != null)
        {
            context.ReplaceGameState(GameStateValue.RoomStarted);
        }

        if (inputContext.newGameTapEntity != null)
        {
            //перезапускаем игру
            context.ReplaceScore(0, context.score.money);

            foreach (var e in characters.GetEntities())
            {
                e.RemoveCharacter();
                e.RemoveCoordinate();
                e.AddDestroyed(1);
            }

            context.ReplaceGameState(GameStateValue.RoomStarted);
        }
    }
}
