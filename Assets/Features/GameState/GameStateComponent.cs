﻿using UnityEngine;
using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game, Unique, Event(EventTarget.Self)]
public class GameStateComponent : IComponent {
    public GameStateValue state;
}

public enum GameStateValue {GameStarted, RoomStarted, RoomPlaying, RoomEnded, GameEnded}