﻿using Entitas;

public sealed class InputCleanupSystem : ICleanupSystem
{
    readonly InputContext context;

    public InputCleanupSystem(Contexts contexts)
    {
        context = contexts.input;
    }

    public void Cleanup()
    {
        context.DestroyAllEntities();
    }
}
