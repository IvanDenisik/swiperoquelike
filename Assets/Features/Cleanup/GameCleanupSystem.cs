﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

public sealed class GameCleanupSystem : ICleanupSystem
{
    readonly IGroup<GameEntity> entitiesWithDestroyComponent;

    public GameCleanupSystem(Contexts contexts)
    {
        entitiesWithDestroyComponent = contexts.game.GetGroup(GameMatcher.Destroyed);
    }

    public void Cleanup()
    {
        foreach (var e in entitiesWithDestroyComponent.GetEntities())
        {
            e.ReplaceDestroyed(e.destroyed.framesTillDestroyed - 1);
            if (e.destroyed.framesTillDestroyed <= 0)
                e.Destroy();
        }
            
    }
}

[Game, Event(EventTarget.Self)]
public sealed class DestroyedComponent : IComponent
{
    public int framesTillDestroyed;
}