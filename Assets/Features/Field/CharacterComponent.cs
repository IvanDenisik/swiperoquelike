﻿using Entitas;
using Entitas.CodeGeneration.Attributes;
using System;

[Serializable, Game, Event(EventTarget.Self)]
public class CharacterComponent : IComponent, ICloneable {
    //Компонент содержит основные данные для персонажа. Начальные значения для этого компонента задаются через инспектор.

    public string id;
    public bool playerOwnsCharacter;
    public int might;
    public int armor;
    public int counters;
    public int abilityPower;

    public int reward;

    public int damageTaken;
    public int healTaken;
    
    public object Clone()
    {
        return MemberwiseClone();
    }
}

[Game]
public class CharacterEffectsComponent : IComponent
{
    //Класс описывает эффекты на персонаже, появляющиеся во время игры

    public bool healed;
    public bool damaged;
}

[Game]
public class CharacterBuffComponent : IComponent
{
    //Класс описывает усиления персонажей.

    public string id;
    public int might;
    public int armor;
    public int counters;
    public int abilityPower;
}