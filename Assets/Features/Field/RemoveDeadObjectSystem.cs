﻿using Entitas;

public sealed class RemoveDeadObjectSystem : IExecuteSystem
{
    readonly GameContext context;
    readonly IGroup<GameEntity> characters;

    public RemoveDeadObjectSystem(Contexts contexts)
    {
        context = contexts.game;
        characters = contexts.game.GetGroup(GameMatcher.Character);
    }

    public void Execute()
    {
        foreach (var e in characters)
            if (e.character.might <= 0 && !e.hasDestroyed)
            {
                e.AddDestroyed(1);
                if (e.character.reward != 0)
                    context.ReplaceScore(context.score.currentLevel, context.score.money + e.character.reward);
            }            
    }
}