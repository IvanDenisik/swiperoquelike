﻿using UnityEngine;
using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game, Unique, Event(EventTarget.Self)]
public class ScoreComponent : IComponent, IScore {

    public int currentLevel { get; set; }
    public int money { get; set; }
}

public interface IScore
{
    int currentLevel { get; }
    int money { get; }
}