﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Entitas;
using System.Linq;

public class ProcessSwipeSystem : IExecuteSystem
{
    //Система реагирует на свайп от игрока

    readonly GameContext context;
    readonly InputContext inputContext;
    readonly IGroup<GameEntity> entitasToMove;
    readonly IGameConfig config;

    public ProcessSwipeSystem(Contexts contexts)
    {
        context = contexts.game;
        inputContext = contexts.input;
        entitasToMove = context.GetGroup(GameMatcher.AllOf(GameMatcher.Character, GameMatcher.Coordinate));
        config = context.gameConfig.value;
    }

    public void Execute()
    {
        var swipeEntity = inputContext.swipeEntity;
        if (swipeEntity == null) return;
        var swipe = swipeEntity.swipe;

        //Определим направление свайпа
        Direction dir = Direction.Right;
        Vector2 swipeVec = swipe.endPointerPos - swipe.startPointerPos;
        if (swipeVec.y > -swipeVec.x && swipeVec.y <= swipeVec.x) dir = Direction.Right;
        if (swipeVec.y > swipeVec.x && swipeVec.y >= -swipeVec.x) dir = Direction.Up;
        if (swipeVec.y >= swipeVec.x && swipeVec.y < -swipeVec.x) dir = Direction.Left;
        if (swipeVec.y <= -swipeVec.x && swipeVec.y < swipeVec.x) dir = Direction.Down;

        //Логика такая, сначала персонажи двигаются, а потом получают урон/бафы/дебафы

        //Пробежимся по клеткам поля и если там есть персонаж, то двигаем его в нужном направлении
        switch (dir)
        {
            case Direction.Right:
                //пробегаемся по клетками справа налево
                for (int x = config.boardSize.x - 1; x >= 0; x--)
                    for (int y = 0; y < config.boardSize.y; y++)
                    {
                        var coord = new Vector2Int(x, y);
                        var entity = FindEntityForCell(coord);
                        if (entity == null) continue;
                        MoveObject(entity, dir, FindTargetEntityForCell(coord, dir));
                    }
                break;
            case Direction.Up:
                //пробегаемся по клетками сверху вниз
                for (int y = config.boardSize.y - 1; y >= 0; y--)
                    for (int x = 0; x < config.boardSize.x; x++)         
                    {
                        var coord = new Vector2Int(x, y);
                        var entity = FindEntityForCell(coord);
                        if (entity == null) continue;
                        MoveObject(entity, dir, FindTargetEntityForCell(coord, dir));
                    }
                break;
            case Direction.Left:
                //пробегаемся по клетками слева направо
                for (int x = 0; x < config.boardSize.x; x++)
                    for (int y = 0; y < config.boardSize.y; y++)
                    {
                        var coord = new Vector2Int(x, y);
                        var entity = FindEntityForCell(coord);
                        if (entity == null) continue;
                        MoveObject(entity, dir, FindTargetEntityForCell(coord, dir));
                    }
                break;
            case Direction.Down:
                //пробегаемся по клетками снизу вверх
                for (int y = 0; y < config.boardSize.y; y++)
                    for (int x = 0; x < config.boardSize.x; x++)                
                    {
                        var coord = new Vector2Int(x, y);
                        var entity = FindEntityForCell(coord);
                        if (entity == null) continue;
                        MoveObject(entity, dir, FindTargetEntityForCell(coord, dir));
                    }
                break;
        }

        //применяем полученный урон/хил
        foreach (var e in entitasToMove)
        {
            var character = e.character;
            character.might += character.healTaken - character.damageTaken;
            character.damageTaken = 0;
            character.healTaken = 0;
            e.ReplaceComponent(GameComponentsLookup.Character, character);
        }

        //выключаем подсказки
        context.ReplaceShowHintEvent(false, null);
    }

    GameEntity FindEntityForCell(Vector2Int coordinate)
    {
        return entitasToMove.GetEntities().ToList().Find(e => e.coordinate.coordinate == coordinate);
    }

    GameEntity FindTargetEntityForCell(Vector2Int coordinate, Direction dir)
    {
        GameEntity e = null;
        switch (dir)
        {
            case Direction.Right:
                //пробегаемся по клетками слева направо
                for (int i = coordinate.x + 1; i < config.boardSize.x; i++)
                {
                    e = FindEntityForCell(new Vector2Int(i, coordinate.y));
                    if (e != null)
                        return e;
                }
                break;
            case Direction.Up:
                //пробегаемся по клетками снизу вверх
                for (int i = coordinate.y + 1; i < config.boardSize.y; i++)
                {
                    e = FindEntityForCell(new Vector2Int(coordinate.x, i));
                    if (e != null)
                        return e;
                }
                break;
            case Direction.Left:
                //пробегаемся по клетками справа налево
                for (int i = coordinate.x - 1; i >= 0; i--)
                {
                    e = FindEntityForCell(new Vector2Int(i, coordinate.y));
                    if (e != null)
                        return e;
                }
                break;
            case Direction.Down:
                //пробегаемся по клетками справа налево
                for (int i = coordinate.y - 1; i >= 0; i--)
                {
                    e = FindEntityForCell(new Vector2Int(coordinate.x, i));
                    if (e != null)
                        return e;
                }
                break;
        }
        return null;
    }

    void MoveObject(GameEntity entity, Direction dir, GameEntity targetEntity)
    {
        //Если нет цели, то упираемся в стенку
        if (targetEntity == null)
            switch (dir)
            {
                case Direction.Right:
                    entity.ReplaceCoordinate(new Vector2Int(config.boardSize.x - 1, entity.coordinate.coordinate.y));
                    break;
                case Direction.Up:
                    entity.ReplaceCoordinate(new Vector2Int(entity.coordinate.coordinate.x, config.boardSize.y - 1));
                    break;
                case Direction.Left:
                    entity.ReplaceCoordinate(new Vector2Int(0, entity.coordinate.coordinate.y));
                    break;
                case Direction.Down:
                    entity.ReplaceCoordinate(new Vector2Int(entity.coordinate.coordinate.x, 0));
                    break;
            }
        else
        {
            //Если цель есть, то упираемся в нее, если только мы не лучник с патронами и цель не враг
            if (!(entity.character.id == "Archer" && entity.character.counters > 0 && entity.character.playerOwnsCharacter != targetEntity.character.playerOwnsCharacter))
                switch (dir)
                {
                    case Direction.Right:
                        entity.ReplaceCoordinate(new Vector2Int(targetEntity.coordinate.coordinate.x - 1, entity.coordinate.coordinate.y));
                        break;
                    case Direction.Up:
                        entity.ReplaceCoordinate(new Vector2Int(entity.coordinate.coordinate.x, targetEntity.coordinate.coordinate.y - 1));
                        break;
                    case Direction.Left:
                        entity.ReplaceCoordinate(new Vector2Int(targetEntity.coordinate.coordinate.x + 1, entity.coordinate.coordinate.y));
                        break;
                    case Direction.Down:
                        entity.ReplaceCoordinate(new Vector2Int(entity.coordinate.coordinate.x, targetEntity.coordinate.coordinate.y + 1));
                        break;
                }

            //Обрабатываем способности
            AtackObject(entity, targetEntity);
            HealObject(entity, targetEntity);
        }
    }

    void AtackObject(GameEntity entity, GameEntity targetEntity)
    {
        //посчитаем расстояние между персонажами
        var vec = targetEntity.coordinate.coordinate - entity.coordinate.coordinate;

        //Атакуем друг друга, если цель враг и мы не лучник, у которого есть каунтеры или враг рядом
        if (!(entity.character.id == "Archer" && entity.character.counters > 0 && Mathf.Abs(vec.x) + Mathf.Abs(vec.y) > 1) 
            && entity.character.playerOwnsCharacter != targetEntity.character.playerOwnsCharacter)
        {
            int dmg = Mathf.Max(targetEntity.character.might - entity.character.armor, 0);
            int dmgForTarget = Mathf.Max(entity.character.might - targetEntity.character.armor, 0);

            var character = entity.character;
            character.damageTaken += dmg;
            entity.ReplaceComponent(GameComponentsLookup.Character, character);

            character = targetEntity.character;
            character.damageTaken += dmgForTarget;
            targetEntity.ReplaceComponent(GameComponentsLookup.Character, character);
        } 

        //Если мы лучник с каунтерами, а цель враг и мы не рядом, то стреляем
        if ((entity.character.id == "Archer" && entity.character.counters > 0 && Mathf.Abs(vec.x) + Mathf.Abs(vec.y) > 1)
            && entity.character.playerOwnsCharacter != targetEntity.character.playerOwnsCharacter)
        {
            int dmgForTarget = Mathf.Max(entity.character.might - targetEntity.character.armor, 0);

            var character = targetEntity.character;
            character.damageTaken += dmgForTarget;
            targetEntity.ReplaceComponent(GameComponentsLookup.Character, character);

            character = entity.character;
            character.counters--;
            entity.ReplaceComponent(GameComponentsLookup.Character, character);
        }
    }

    void HealObject(GameEntity entity, GameEntity targetEntity)
    {
        //Если мы паладин и цель союзник и есть каунтеры, то хиляем
        if (entity.character.id == "Paladin" && entity.character.counters > 0
            && entity.character.playerOwnsCharacter == targetEntity.character.playerOwnsCharacter)
        {
            var character = entity.character;
            character.counters--;
            entity.ReplaceComponent(GameComponentsLookup.Character, character);

            character = targetEntity.character;
            character.healTaken += config.paladinHeal;
            targetEntity.ReplaceComponent(GameComponentsLookup.Character, character);
        }
    }

    public enum Direction { Up, Down, Left, Right }
}