﻿using UnityEngine;
using TMPro;
using Entitas;
using Entitas.Unity;

public class ScoreView : MonoBehaviour, IScoreListener {

    [SerializeField] TextMeshProUGUI levelText;
    [SerializeField] TextMeshProUGUI moneyText;

    public void Start()
    {
        var e = Contexts.sharedInstance.game.scoreEntity;
        gameObject.Link(e);
        e.AddScoreListener(this);

        levelText.text = "Room: " + e.score.currentLevel;
        moneyText.text = "Money: " + e.score.money;
    }

    public void OnScore(GameEntity entity, int level, int money)
    {
        levelText.text = "Room: " + level;
        moneyText.text = "Money: " + money;
    }
}
