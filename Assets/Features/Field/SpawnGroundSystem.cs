﻿using System.Collections.Generic;
using UnityEngine;
using Entitas;

public class SpawnGroundSystem : IInitializeSystem
{
    readonly GameContext context;

    public SpawnGroundSystem(Contexts contexts)
    {
        context = contexts.game;
    }

    public void Initialize()
    {
        var config = context.gameConfig;

        for (int i = 0; i < config.value.boardSize.x; i++)
            for (int j = 0; j < config.value.boardSize.y; j++)
            {
                var e = context.CreateEntity();
                e.AddCoordinate(new Vector2Int(i, j));
                e.AddAsset("Ground");
            }
    }
}