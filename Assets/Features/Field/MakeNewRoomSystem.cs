﻿using System.Collections.Generic;
using UnityEngine;
using Entitas;
using System.Linq;

public static class FieldHelper
{
    public static int GetEmptyCell(out Vector2Int coordinate)
    {
        coordinate = Vector2Int.zero;

        var context = Contexts.sharedInstance.game;
        var config = context.gameConfig;
        var group = context.GetGroup(GameMatcher.Character).GetEntities().Select(e => e.coordinate.coordinate).ToList();

        int randId = Random.Range(0, config.value.boardSize.x * config.value.boardSize.y - group.Count);
        int count = 0;
        for (int x = 0; x < config.value.boardSize.x; x++)
            for (int y = 0; y < config.value.boardSize.y; y++)
            {
                coordinate = new Vector2Int(x, y);
                if (group.Contains(coordinate))
                    continue;
                else
                {
                    if (count == randId)
                        return 1;
                    count++;
                }            
            }

        return -1;
    }
}

public class MakeNewRoomSystem : IInitializeSystem, IExecuteSystem
{
    //Система спавнит / уничтожает объекты при переходе к новой комнате

    readonly GameContext context;
    readonly GameConfigComponent config;
    readonly IGroup<GameEntity> characters;
    readonly IGroup<GameEntity> characterBuffs;

    int allyRatesSum = 0;
    int enemyRatesSum = 0;

    public MakeNewRoomSystem(Contexts contexts)
    {
        context = contexts.game;
        config = context.gameConfig;
        characters = context.GetGroup(GameMatcher.Character);
        characterBuffs = context.GetGroup(GameMatcher.CharacterBuff);
    }

    public void Initialize()
    {
        //подсчитаем суммы которые помогут определить вероятность появления персонажей
        allyRatesSum = 0;
        enemyRatesSum = 0;
        for (int i = 0; i < config.value.characters.Length; i++)
            if (config.value.characters[i].playerOwnsCharacter)
                allyRatesSum += config.value.rates[i];
            else
                enemyRatesSum += config.value.rates[i];

        //заполняем комнату
        MakeRoom();
    }

    public void Execute()
    {
        if (context.gameState.state == GameStateValue.RoomStarted)
            MakeRoom();
    }

    void MakeRoom()
    {
        //Уничтожаем все сущности, кроме персонажей игрока
        foreach (var e in characters.GetEntities())
        {
            if (!e.character.playerOwnsCharacter)
            {
                e.AddDestroyed(1);
                e.RemoveCharacter();
                e.RemoveCoordinate();
            }               
        }

        //Добавляем счетчик уровня
        context.ReplaceScore(context.score.currentLevel + 1, context.score.money);

        if (characters.count == 0)
        {
            //если поле пустое, то создаем союзников
            for (int i = 0; i < 3; i++)
                if (Spawn(false, allyRatesSum) == -1)
                    break;
        }
        else
        {
            //если поле НЕ пустое, то передвигаем союзников
            Vector2Int vec = Vector2Int.zero;
            foreach (var e in characters)
            {
                if (FieldHelper.GetEmptyCell(out vec) == -1) return;

                e.ReplaceCoordinate(vec);

                var character = e.character;
                if (character.id == "Archer" || character.id == "Paladin")
                {
                    character.counters++;
                    e.ReplaceComponent(GameComponentsLookup.Character, character);
                }
            }
        }
        
        //спавним врагов
        for (int i = 0; i < 3; i++)
            if (Spawn(true, enemyRatesSum) == -1)
                break;

        context.gameState.state = GameStateValue.RoomPlaying;
    }

    int Spawn(bool isEnemy, int rateSumForCharacters)
    {
        var e = context.CreateEntity();

        //Определяем позицию персонажа
        Vector2Int vec = Vector2Int.zero;
        if (FieldHelper.GetEmptyCell(out vec) == -1) return -1;
        e.AddCoordinate(vec);

        //Определяем тип персонажа
        int rand = Random.Range(0, rateSumForCharacters);
        int rateSum = 0;
        for (int j = 0; j < config.value.characters.Length; j++)
        {
            if (config.value.characters[j].playerOwnsCharacter == isEnemy) continue;

            rateSum += config.value.rates[j];
            if (rand < rateSum)
            {
                e.AddAsset(config.value.characters[j].id);
                var characterComponent = (CharacterComponent)config.value.characters[j].Clone();
                if (characterComponent.playerOwnsCharacter)
                {
                    var characterBuff = characterBuffs.GetEntities().ToList().Find(ent => ent.characterBuff.id == characterComponent.id).characterBuff;
                    characterComponent.might += characterBuff.might;
                    characterComponent.armor += characterBuff.armor;
                    characterComponent.counters += characterBuff.counters;
                    characterComponent.abilityPower += characterBuff.abilityPower;
                }               
                e.AddComponent(GameComponentsLookup.Character, characterComponent);
                e.AddComponent(GameComponentsLookup.CharacterEffects, new CharacterEffectsComponent());
                break;
            }
        }
        return 1;
    }
}