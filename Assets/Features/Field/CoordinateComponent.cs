﻿using UnityEngine;
using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game, Event(EventTarget.Self)]
public class CoordinateComponent : IComponent
{
    public Vector2Int coordinate; //Позиция объекта на поле в координатах поля
}
