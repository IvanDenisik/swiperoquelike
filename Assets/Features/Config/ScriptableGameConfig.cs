using UnityEngine;
using Entitas.CodeGeneration.Attributes;
using System.Collections.Generic;

[CreateAssetMenu(menuName = "SwipeRogueLike/Game Config")]
public class ScriptableGameConfig : ScriptableObject, IGameConfig
{
    [SerializeField] Vector2Int _boardSize; public Vector2Int boardSize => _boardSize; //������ ���� � �������
    [SerializeField] Vector2 _cellSize; public Vector2 cellSize => _cellSize; //������ ������ � ����������� Unity
    [SerializeField] float _swipeMinLength; public float swipeMinLength => _swipeMinLength; //����������� ����� ������
    [SerializeField] CharacterComponent[] _characters; public CharacterComponent[] characters => _characters; //��������� ��� ����� ������
    [SerializeField] int[] _rates; public int[] rates => _rates; //�����, ������������ ����������� ��������� ���� ��������� �� ����
    [SerializeField] int _paladinHeal; public int paladinHeal => _paladinHeal; //����� �� ������� ������ �������
    [SerializeField] float _swipeSpeed; public float swipeSpeed => _swipeSpeed; //�������� �������� ������ ��������� ��� ������
}

[Game, Unique, ComponentName("GameConfig")]
public interface IGameConfig
{
    Vector2Int boardSize { get; }
    Vector2 cellSize { get; }
    float swipeMinLength { get; }
    CharacterComponent[] characters { get; }
    int[] rates { get; }
    int paladinHeal { get; }
    float swipeSpeed { get; }
}

