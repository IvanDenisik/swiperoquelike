﻿using System;
using Entitas;

/**
 *
 * The GameController creates and manages all systems in Match One
 *
 */

public class GameController
{
    readonly Systems _systems;

    public GameController(Contexts contexts, IGameConfig gameConfig)
    {
        contexts.game.SetGameConfig(gameConfig);
        contexts.game.SetGameState(GameStateValue.GameStarted);
        contexts.game.SetScore(0, 0);
        contexts.game.SetShowHintEvent(false, null);

        contexts.game.CreateEntity().AddCharacterBuff("Warrior", 0, 0, 0, 0);
        contexts.game.CreateEntity().AddCharacterBuff("Paladin", 0, 0, 0, 0);
        contexts.game.CreateEntity().AddCharacterBuff("Archer", 0, 0, 0, 0);

        // This is the heart of Swipe Roque Like:
        // All logic is contained in all the sub systems of GameSystems
        _systems = new GameSystems(contexts);
    }

    public void Initialize()
    {
        // This calls Initialize() on all sub systems
        _systems.Initialize();
    }

    public void Execute()
    {
        // This calls Execute() and Cleanup() on all sub systems
        _systems.Execute();
        _systems.Cleanup();
    }
}
