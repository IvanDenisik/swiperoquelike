using UnityEngine;

/**
 *
 * GameControllerBehaviour is the entry point to Swipe Roque Like
 *
 * The only purpose of this class is to redirect and forward
 * the Unity lifecycle events to the GameController
 *
 */

public class GameControllerBehaviour : MonoBehaviour
{
    public ScriptableGameConfig gameConfig;

    GameController _gameController;

    void Awake()
    {
        _gameController = new GameController(Contexts.sharedInstance, gameConfig);

        //Initialize ���������� � Awake, ����� � Start ����� ���� ������������ ������� �������� �� ECS.
        _gameController.Initialize(); 
    }

    void Start()
    {

    }

    //Execute ���������� � LateUpdate, ����� � update ����� ���� ���������� ������ ��� ��������� ECS � 
    //����� ������� ����� ���������� ����� ������.
    void LateUpdate() => _gameController.Execute();  
}
