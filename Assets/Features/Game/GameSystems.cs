﻿public sealed class GameSystems : Feature
{
    public GameSystems(Contexts contexts)
    {
        //Initialize
        Add(new SpawnGroundSystem(contexts));

        // Execute
        Add(new ChangeGameStateSystem(contexts));
        Add(new MakeNewRoomSystem(contexts));
        Add(new ProcessSwipeSystem(contexts));
        Add(new RemoveDeadObjectSystem(contexts));
        Add(new BuffCharacterSystem(contexts));

        // Reactive     
        Add(new AddViewSystem(contexts));

        // Events (Generated)
        //Add(new InputEventSystems(contexts));
        Add(new GameEventSystems(contexts));

        // Cleanup    
        Add(new InputCleanupSystem(contexts));
        Add(new GameCleanupSystem(contexts));
    }
}
