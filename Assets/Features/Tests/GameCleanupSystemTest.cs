﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Entitas;

public class GameCleanupSystemTest : MonoBehaviour {

    GameEntity e;

    [ContextMenu("CreateEntitieWithView")]
    void CreateEntitieWithView() {
        e = Contexts.sharedInstance.game.CreateEntity();
        e.AddAsset("View");
    }

    [ContextMenu("AddDestroyComponent")]
    void AddDestroyComponent()
    {
        e.AddDestroyed(1);
    }
}
