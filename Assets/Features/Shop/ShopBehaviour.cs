﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopBehaviour : MonoBehaviour {

    [SerializeField] Button warriorMightBtn;
    [SerializeField] Button warriorArmorBtn;
    [SerializeField] Button paladinMightBtn;
    [SerializeField] Button paladinMHealBtn;
    [SerializeField] Button paladinCountersBtn;

    void Start () {
        warriorMightBtn.onClick.AddListener(AddWarriorMight);

    }

    void AddWarriorMight()
    {
        var comp = new CharacterBuffEventComponent();
        comp.id = "Warrior";
        comp.might = 1;
        Contexts.sharedInstance.input.CreateEntity().AddComponent(InputComponentsLookup.CharacterBuffEvent, comp);
    }
}
